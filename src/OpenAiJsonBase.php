<?php

namespace Drupal\ai_interpolator_openai;

use Drupal\ai_interpolator\PluginBaseClasses\TextToJsonField;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Base for the three OpenAI JSON field types.
 */
class OpenAiJsonBase extends TextToJsonField implements AiInterpolatorFieldRuleInterface {

  // Load all the logic.
  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI JSON Field';

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);

    $extraForm = $this->loadExtraAdvancedFormFields($entity, $fieldDefinition);
    $form = array_merge($form, $extraForm);

    $form['interpolator_store_full_request'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Store full request'),
      '#description' => $this->t('Store the full request in the field, so this would include meta data from the OpenAi.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_store_full_request', FALSE),
      '#weight' => 24,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = parent::generate($entity, $fieldDefinition, $interpolatorConfig);
    // Store the whole thing.
    if ($interpolatorConfig['store_full_request']) {
      foreach ($values as $key => $value) {
        $values[$key] = json_encode($value);
      }
    }
    else {
      foreach ($values as $key => $value) {
        $jsonText = $value['choices'][0]['message']['content'];
        // Match json on the first [ or { and on the last } or ]
        preg_match('/[\[\{].*[\}\]]/s', $jsonText, $matches);
        if (!empty($matches[0])) {
          $values[$key] = $matches[0];
        }
      }
    }
    return $values;
  }
}
