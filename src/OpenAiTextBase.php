<?php

namespace Drupal\ai_interpolator_openai;

use Drupal\ai_interpolator\PluginBaseClasses\ComplexTextChat;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Helper for text bases.
 */
class OpenAiTextBase extends ComplexTextChat implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Text Long';

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = $this->loadExtraAdvancedFormFields($entity, $fieldDefinition);
    $this->getGeneralHelper()->addJoinerConfigurationFormField('interpolator_openai', $form, $entity, $fieldDefinition);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = parent::generate($entity, $fieldDefinition, $interpolatorConfig);
    // If we should join, we do that.
    if (isset($interpolatorConfig['openai_joiner']) && $interpolatorConfig['openai_joiner']) {
      $joiner = $interpolatorConfig['openai_joiner'];
      if ($joiner == 'other') {
        $joiner = $interpolatorConfig['openai_joiner_other'];
      }
      // Reset values.
      $values = [$this->getGeneralHelper()->joinValues($values, $joiner)];
    }
    return $values;
  }

}
