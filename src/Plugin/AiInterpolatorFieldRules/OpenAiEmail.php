<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Email;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a E-Mail field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_email",
 *   title = @Translation("OpenAI E-Mail"),
 *   field_rule = "email"
 * )
 */
class OpenAiEmail extends Email implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI E-Mail';

}
