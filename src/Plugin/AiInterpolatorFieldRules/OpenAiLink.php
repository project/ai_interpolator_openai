<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Link;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a Link field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_link",
 *   title = @Translation("OpenAI Link"),
 *   field_rule = "link"
 * )
 */
class OpenAiLink extends Link implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Link';

}
