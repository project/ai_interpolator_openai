<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Lists;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a list float field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_list_float",
 *   title = @Translation("OpenAI List Float"),
 *   field_rule = "list_float"
 * )
 */
class OpenAiListFloat extends Lists implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI List Float';

}
