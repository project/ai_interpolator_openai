<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\OfficeHours;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a office_hours field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_office_hours",
 *   title = @Translation("OpenAI Office Hours"),
 *   field_rule = "office_hours"
 * )
 */
class OpenAiOfficeHours extends OfficeHours implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Office Hours';

}
