<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Lists;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a list string field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_list_string",
 *   title = @Translation("OpenAI List String"),
 *   field_rule = "list_string"
 * )
 */
class OpenAiListString extends Lists implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI List String';

}
