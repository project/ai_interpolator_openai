<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiJsonBase;

/**
 * The rules for a json_native_binary field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_json_native_binary",
 *   title = @Translation("OpenAI JSON Field"),
 *   field_rule = "json_native_binary"
 * )
 */
class OpenAiJsonNativeBinary extends OpenAiJsonBase implements AiInterpolatorFieldRuleInterface {

}

