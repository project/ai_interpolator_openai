<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Numeric;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for an decimal field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_decimal",
 *   title = @Translation("OpenAI Decimal"),
 *   field_rule = "decimal"
 * )
 */
class OpenAiDecimal extends Numeric implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Decimal';

}
