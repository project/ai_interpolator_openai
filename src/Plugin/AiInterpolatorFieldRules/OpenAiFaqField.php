<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\FaqField;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a FAQ field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_faq",
 *   title = @Translation("OpenAI FAQ Field"),
 *   field_rule = "faqfield"
 * )
 */
class OpenAiFaqField extends FaqField implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI FAQ Field';

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "Based on the context text return 5 questions and answers.\n\nContext:\n{{ context }}";
  }

}
