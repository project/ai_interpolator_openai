<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\EntityReference;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for an entity reference field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_entity_reference",
 *   title = @Translation("OpenAI Entity Reference"),
 *   field_rule = "entity_reference",
 *   target = "any"
 * )
 */
class OpenAiEntityReference extends EntityReference implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Entity Reference';

}
