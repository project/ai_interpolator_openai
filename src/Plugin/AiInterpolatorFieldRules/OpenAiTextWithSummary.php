<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTextBase;

/**
 * The rules for a text with summary field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_text_with_summary",
 *   title = @Translation("OpenAI Text with Summary"),
 *   field_rule = "text_with_summary"
 * )
 */
class OpenAiTextWithSummary extends OpenAiTextBase implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Text With Summary';

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "Based on the context text create a summary between 100 and 300 characters.\n\nContext:\n{{ context }}";
  }

}
