<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\CustomField;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for the custom field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_custom_field",
 *   title = @Translation("OpenAI Custom Field"),
 *   field_rule = "custom"
 * )
 */
class OpenAiCustomField extends CustomField implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Custom Field';

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $baseForm = $this->addCustomFormFields('openai', $entity, $fieldDefinition);
    $form = $this->loadExtraAdvancedFormFields($entity, $fieldDefinition);
    $form = array_merge($baseForm, $form);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = $this->generatePrompts('openai', $entity, $fieldDefinition, $interpolatorConfig);
    return $values;
  }
}
