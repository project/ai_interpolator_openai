<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Telephone;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a Telephone field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_telephone",
 *   title = @Translation("OpenAI Telephone"),
 *   field_rule = "telephone"
 * )
 */
class OpenAiTelephone extends Telephone implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Telephone';

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "Based on the context text return all telephone numbers listed with a country code added.\n\nContext:\n{{ context }}";
  }

}
