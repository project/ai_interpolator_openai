<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Exceptions\AiInterpolatorResponseErrorException;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiVideoHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\file\Entity\File;

/**
 * The rules for a video to image field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_video_to_image",
 *   title = @Translation("OpenAI Video To Image (Experimental)"),
 *   field_rule = "entity_reference",
 *   target = "media",
 * )
 */
class OpenAiVideoToMediaImage extends OpenAiVideoHelper implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Thumbnail Extraction (Experimental)';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function ruleIsAllowed(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    // Checks system for ffmpeg, otherwise this rule does not exist.
    $command = (PHP_OS == 'WINNT') ? 'where ffmpeg' : 'which ffmpeg';
    $result = shell_exec($command);
    return $result ? TRUE : FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function tokens() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return [
      'file',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $options = [];
    $types = $this->entityTypeBundleInfo->getBundleInfo('media');
    foreach ($types as $key => $type) {
      $options[$key] = $type['label'];
    }

    $form['interpolator_openai_media_type'] = [
      '#type' => 'select',
      '#title' => 'Media Type',
      '#description' => $this->t('Media Type to set'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_openai_media_type', 'image'),
    ];

    $form['interpolator_cutting_prompt'] = [
      '#type' => 'textarea',
      '#title' => 'Cutting Prompt',
      '#description' => $this->t('Any commands that you need to give to cut out the image(s).Can use Tokens if Token module is installed.'),
      '#attributes' => [
        'placeholder' => $this->t('Cut out an image where they show two people holding hands.'),
      ],
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_cutting_prompt', ''),
      '#weight' => 24,
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      // Because we have to invoke this only if the module is installed, no
      // dependency injection.
      // @codingStandardsIgnoreLine @phpstan-ignore-next-line
      $form['interpolator_cutting_prompt_token_help'] = \Drupal::service('token.tree_builder')->buildRenderable([
        $this->getEntityTokenType($entity->getEntityTypeId()),
        'current-user',
      ]);
      $form['interpolator_cutting_prompt_token_help']['#weight'] = 25;
    }
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    // Don't get the base form.
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    // Tokenize prompt.
    $cutPrompt = $this->renderTokenPrompt($interpolatorConfig['cutting_prompt'], $entity);

    $total = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $entityWrapper) {
      if ($entityWrapper->entity) {
        $fileEntity = $entityWrapper->entity;
        if (in_array($fileEntity->getMimeType(), [
          'video/mp4',
          'video/webm',
          'video/x-matroska',
          'video/mpeg',
        ])) {
          $this->prepareToExplain($entityWrapper->entity);
          $prompt = "The following images shows rasters of scenes from a video together with a timestamp when it happens in the video. The audio is transcribed below. Please follow the instructions below with the video as context, using images and transcripts and try to figure out what image or images the person wants to cut out. Give back multiple timestamps if multiple images are wanted.\n\n";
          $prompt .= "Instructions:\n----------------------------\n" . $cutPrompt . "\n----------------------------\n\n";
          $prompt .= "Transcription:\n----------------------------\n" . $this->transcription . "\n----------------------------\n\n";
          $prompt .= "\n\nDo not include any explanations, only provide a RFC8259 compliant JSON response following this format without deviation.\n[{\"value\": [{\"timestamp\": \"The timestamp to take an image in format h:i:s.ms\"}]].";
          $values = $this->openAi->generateResponse($prompt, $fieldDefinition, [
            'openai_model' => 'gpt-4o',
          ], $this->images);
          // Run a second time to get the exact shot.
          $this->createVideoRasterImages($entityWrapper->entity, $values[0][0]['timestamp']);
          $values = $this->openAi->generateResponse($prompt, $fieldDefinition, [
            'openai_model' => 'gpt-4o',
          ], $this->images);

          $total = array_merge_recursive($total, $values);
        }
      }
    }
    return $total;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should have start and end time.
    if (!is_array($value) && !isset($value[0]['timestamp'])) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $mediaStorage = $this->entityManager->getStorage('media');
    $thirdPart = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySettings('ai_interpolator');
    $mediaType = $thirdPart['interpolator_openai_media_type'];
    $mediaTypeInterface = $this->entityManager->getStorage('media_type')->load($mediaType);
    /** @var \Drupal\media\Entity\Media */
    $media = $mediaStorage->create([
      'name' => 'tmp',
      'bundle' => $mediaType,
    ]);
    $sourceField = $media->getSource()->getSourceFieldDefinition($mediaTypeInterface);
    $fileField = $sourceField->getName();
    $mediaFields = $this->fieldManager->getFieldDefinitions('media', $mediaType);
    $config = $mediaFields[$fileField] ? $mediaFields[$fileField]->getConfig($entity->bundle())->getSettings() : NULL;
    if (!$config) {
      return [];
    }
    $mediaEntitities = [];
    // Create a tmp directory.
    $this->createTempDirectory();

    // First cut out the videos.
    $baseField = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_base_field', '');
    $realPath = $this->fileSystem->realpath($entity->{$baseField}->entity->getFileUri());
    // Get the actual file name and replace it with _cut.
    $fileName = pathinfo($realPath, PATHINFO_FILENAME) . '.jpg';
    $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/' . $fileName;

    foreach ($values as $keys) {
      $tmpNames = [];
      foreach ($keys as $key) {
        // Generate double files, but we only need the last one.
        $tmpName = $this->fileSystem->tempnam($this->tmpDir, 'video') . '.jpg';
        $tmpNames[] = $tmpName;
        $inputVideo = $this->video ?? $realPath;
        $command = 'ffmpeg -y -nostdin -i "' . $inputVideo . '" -ss "' . $key['timestamp'] . '" -frames:v 1 ' . $tmpName;
        exec($command, $status);
        if ($status) {
          throw new AiInterpolatorResponseErrorException('Could not generate new videos.');
        }
      }

      foreach ($tmpNames as $tmpName) {
        // Move the file to the correct place.
        $fixedFile = $this->fileSystem->move($tmpName, $filePath);

        // Generate the new file entity.
        $file = File::create([
          'uri' => $fixedFile,
          'status' => 1,
          'uid' => $this->currentUser->id(),
        ]);
        $file->save();

        $resolution = getimagesize($fixedFile);
        // Add to the entities saved.
        $fileForMedia = [
          'target_id' => $file->id(),
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];

        $media = $mediaStorage->create([
          'name' => basename($fixedFile),
          'bundle' => $mediaType,
          $fileField => $fileForMedia,
        ]);
        $media->save();

        $mediaEntitities[] = $media->id();
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $mediaEntitities);
    return TRUE;
  }
}
