<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiSimpleTextBase;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_simple_string_long",
 *   title = @Translation("OpenAI Simple Text"),
 *   field_rule = "string_long"
 * )
 */
class OpenAiSimpleStringLong extends OpenAiSimpleTextBase implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "Based on the context text create a summary under {{ max_length }} characters.\n\nContext:\n{{ context }}";
  }
}
