<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Boolean;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for an boolean field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_boolean",
 *   title = @Translation("OpenAI Boolean"),
 *   field_rule = "boolean"
 * )
 */
class OpenAiBoolean extends Boolean implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Boolean';

}
