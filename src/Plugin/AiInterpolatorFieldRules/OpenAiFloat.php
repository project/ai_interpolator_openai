<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Numeric;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for an float field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_float",
 *   title = @Translation("OpenAI Float"),
 *   field_rule = "float"
 * )
 */
class OpenAiFloat extends Numeric implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Float';

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "Based on the context text add a sentiment rating between {{ min }} and {{ max }}, where {{ min }} means really negative sentiment and {{ max }} means really great sentiment. You can answer with decimals as well for more exactness.\n\nContext:\n{{ context }}";
  }

}
