<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Chart;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for an charts field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_charts_text",
 *   title = @Translation("OpenAi Charts From Text"),
 *   field_rule = "chart_config"
 * )
 */
class OpenAiChartFromText extends Chart {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAi Charts From Text';

}
