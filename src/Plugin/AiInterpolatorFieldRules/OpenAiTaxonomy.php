<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Taxonomy;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiRequester;
use Drupal\ai_interpolator_openai\OpenAiTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for a taxonomy field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_taxonomy",
 *   title = @Translation("OpenAI Taxonomy"),
 *   field_rule = "entity_reference",
 *   target = "taxonomy_term"
 * )
 */
class OpenAiTaxonomy extends Taxonomy implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Taxonomy';

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);
    $extraForm = $this->loadExtraAdvancedFormFields($entity, $fieldDefinition);
    return array_merge_recursive($extraForm, $form);
  }

}
