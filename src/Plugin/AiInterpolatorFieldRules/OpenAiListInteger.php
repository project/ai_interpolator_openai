<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginBaseClasses\Lists;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;

/**
 * The rules for a list integer field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_list_integer",
 *   title = @Translation("OpenAI List Integer"),
 *   field_rule = "list_integer"
 * )
 */
class OpenAiListInteger extends Lists implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI List Integer';

}
