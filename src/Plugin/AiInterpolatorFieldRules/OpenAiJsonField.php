<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiJsonBase;

/**
 * The rules for a json field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_json",
 *   title = @Translation("OpenAI JSON Field"),
 *   field_rule = "json"
 * )
 */
class OpenAiJsonField extends OpenAiJsonBase implements AiInterpolatorFieldRuleInterface {

}
