<?php

namespace Drupal\ai_interpolator_openai\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiSimpleTextBase;

/**
 * The rules for a text_long_with_summary field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_openai_simple_text_long_with_summary",
 *   title = @Translation("OpenAI Simple Text"),
 *   field_rule = "text_long_with_summary"
 * )
 */
class OpenAiSimpleTextLongWithSummary extends OpenAiSimpleTextBase implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "Based on the context text create a summary under {{ max_length }} characters.\n\nContext:\n{{ context }}";
  }
}
