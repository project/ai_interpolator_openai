<?php

namespace Drupal\ai_interpolator_openai;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * OpenAI API trait.
 */
trait OpenAiTrait {

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    return $this->loadExtraAdvancedFormFields($entity, $fieldDefinition);
  }

  /**
   * Loads the form fields in another method, to be available in the trait user.
   */
  public function loadExtraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);
    // Get the models.
    $form = $this->getModelsForForm($fieldDefinition, $entity);
    // Add common LLM parameters.
    $this->getGeneralHelper()->addCommonLlmParametersFormFields('openai_options', $form, $entity, $fieldDefinition);
    // More Tokens, since its OpenAI.
    $form['interpolator_openai_options_max_tokens']['#default_value'] = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_openai_options_max_tokens', '2048');
    // Top k is not exposed by OpenAI.
    unset($form['interpolator_openai_options_top_k']);
    return $form;
  }

  /**
   * Get all models in Fireworksai for the form.
   *
   * @return array
   *   The response.
   */
  public function getModelsForForm(FieldDefinitionInterface $fieldDefinition, ContentEntityInterface $entity, $chat = TRUE) {
    $form['interpolator_openai_model'] = [
      '#type' => 'select',
      '#title' => 'OpenAI Model',
      '#description' => $this->t('Choose the model you want to use here.'),
      '#options' => \Drupal::service('ai_interpolator_openai.request')->getModels(),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_openai_model', 'gpt-4-turbo'),
      '#weight' => 24,
    ];

    // Offer to upload an image.
    $options = ['' => 'No image field'] + $this->getFieldsOfType($entity, 'image');
    $form['interpolator_openai_vision_images'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => 'OpenAI Vision Image',
      '#description' => $this->t('A image field to use for OpenAI Vision.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_openai_vision_images', ''),
      '#weight' => 24,
      '#states' => [
        'visible' => [
          ':input[name="interpolator_openai_model"]' => [
            'value' => 'gpt-4-vision-preview',
          ],
          'or',
          ':input[name="interpolator_openai_model"]' => [
            'value' => 'gpt-4o',
          ],
        ],
      ],
    ];

    // Get image styles on image field if an image field is chosen.
    $imageStyles = $this->getGeneralHelper()->getImageStyles();
    $form['interpolator_openai_vision_image_style'] = [
      '#type' => 'select',
      '#options' => $imageStyles,
      '#title' => 'OpenAI Vision Image Style',
      '#description' => $this->t('An image style to use as preprocessor for OpenAI Vision. This is good to not rake up the cost of OpenAI Vision, using too big images.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_openai_vision_image_style', ''),
      '#weight' => 24,
      '#states' => [
        'visible' => [
          ':input[name="interpolator_openai_vision_images"]' => [
            '!value' => '',
          ],
        ]
      ],
    ];

    $form['interpolator_openai_role'] = [
      '#type' => 'textarea',
      '#title' => 'OpenAI Role',
      '#description' => $this->t('If the AI should have some specific role, write it here.'),
      '#attributes' => [
        'placeholder' => $this->t('A receptionist.'),
      ],
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_openai_role', ''),
      '#weight' => 25,
    ];

    $form['interpolator_openai_moderation'] = [
      '#type' => 'checkbox',
      '#title' => 'OpenAI Moderation',
      '#description' => $this->t('If OpenAI should run through moderation request, before sending AI request. Highly recommended.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_openai_moderation', 1),
      '#weight' => 25,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generateResponse($prompt, $interpolatorConfig, ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $openAiRequester = \Drupal::service('ai_interpolator_openai.request');
    // Run moderation first, if wanted.
    if (!empty($interpolatorConfig['openai_moderation'])) {
      if ($openAiRequester->hasFlaggedContent($prompt, $interpolatorConfig)) {
        return 'Content was flagged by OpenAI.';
      }
    }
    // Get options.
    $options = [];
    foreach ($interpolatorConfig as $key => $value) {
      if (strpos($key, 'openai_options_') === 0) {
        if ($value) {
          $options[str_replace('openai_options_', '', $key)] = $value;
        }
      }
    }
    $images = [];
    if (!empty($interpolatorConfig['openai_vision_images']) && $entity->{$interpolatorConfig['openai_vision_images']}->entity) {
      foreach ($entity->{$interpolatorConfig['openai_vision_images']} as $imageBase)  {
        $imageEntity = $imageBase->entity;
        // Check if we should preprocess the image.
        if (!empty($interpolatorConfig['openai_vision_image_style'])) {
          $imageEntity = $this->getGeneralHelper()->preprocessImageStyle($imageEntity, $interpolatorConfig['openai_vision_image_style']);
        }
        $images[] = $this->getGeneralHelper()->base64EncodeFileEntity($imageEntity);
      }
    }
    $response = $openAiRequester->generateResponse($prompt, $fieldDefinition, $interpolatorConfig, $images, $options);
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function generateRawResponse($prompt, $interpolatorConfig, ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $openAiRequester = \Drupal::service('ai_interpolator_openai.request');
    // Run moderation first, if wanted.
    if (!empty($interpolatorConfig['openai_moderation'])) {
      if ($openAiRequester->hasFlaggedContent($prompt, $interpolatorConfig)) {
        return 'Content was flagged by OpenAI.';
      }
    }
    $imageEntity = !empty($interpolatorConfig['openai_vision_images']) ? $entity->{$interpolatorConfig['openai_vision_images']}->entity : NULL;
    $images = !empty($imageEntity) ? $this->getGeneralHelper()->base64EncodeFileEntity($imageEntity) : [];
    $response = $openAiRequester->openAiChatRequest($prompt, $interpolatorConfig, $images);
    return $response;
  }

}
