<?php

namespace Drupal\ai_interpolator_openai;

use Drupal\ai_interpolator\PluginBaseClasses\SimpleTextChat;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_openai\OpenAiTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Helper for text bases.
 */
class OpenAiSimpleTextBase extends SimpleTextChat implements AiInterpolatorFieldRuleInterface {

  use OpenAiTrait;

  /**
   * {@inheritDoc}
   */
  public $title = 'OpenAI Simple Text';

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = $this->loadExtraAdvancedFormFields($entity, $fieldDefinition);
    return $form;
  }

}
